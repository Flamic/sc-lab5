﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Delivery.Model;
using Newtonsoft.Json;

namespace Delivery.Serializer
{
    class DeliverySerializer
    {
        public static void Serialize(string filePath, MainModel data)
        {
            var str = JsonConvert.SerializeObject(data);
            File.WriteAllText(filePath, str);
        }
        public static void Deserialize(string filePath, out MainModel data)
        {
            try
            {
                data = JsonConvert.DeserializeObject<MainModel>(File.ReadAllText(filePath));
            }
            catch (Exception)
            {
                data = new MainModel();
                data.LoadMockupData();
            }
        }
    }
}
