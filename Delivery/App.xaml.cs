﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Media;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using Delivery.Model;
using Delivery.Serializer;
using Delivery.ViewModel;

namespace Delivery
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private const string FILE_PATH = "delivery.json";
        private readonly MainViewModel _mainViewModel;
        private MainModel _mainModel;
        private readonly IMapper _mapper;

        public App()
        {
            DeliverySerializer.Deserialize(FILE_PATH, out _mainModel);
            _mapper = Mapping.Mapping.Create();
            _mainViewModel = _mapper.Map<MainModel, MainViewModel>(_mainModel);

            var mainWindow = new MainWindow() { DataContext = _mainViewModel };
            mainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs args)
        {
            try
            {
                _mainModel = _mapper.Map<MainViewModel, MainModel>(_mainViewModel);
                DeliverySerializer.Serialize(FILE_PATH, _mainModel);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    $"Cannot save to file '{FILE_PATH}'!\n{e.Message}",
                    "Saving error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                base.OnExit(null);
            }
        }
    }
}
