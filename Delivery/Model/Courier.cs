﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.Model
{
    [Serializable]
    class Courier : Person
    {
        public string CarLicensePlate { get; set; }
    }
}
