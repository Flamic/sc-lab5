﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.Model
{
    [Serializable]
    class Order : Base
    {
        public int CourierId { get; set; }
        public int CustomerId { get; set; }
        public string Product { get; set; }
        public string Address { get; set; }
        public double Cost { get; set; }
        public DateTime ReceiveTime { get; set; }
    }
}
