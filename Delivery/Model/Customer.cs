﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.Model
{
    [Serializable]
    class Customer : Person
    {
        public bool Blacklisted { get; set; }
    }
}
