﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.Model
{
    [Serializable]
    class MainModel
    {
        public ObservableCollection<Courier> Couriers { get; set; } = new ObservableCollection<Courier>();
        public ObservableCollection<Customer> Customers { get; set; } = new ObservableCollection<Customer>();
        public ObservableCollection<Order> Orders { get; set; } = new ObservableCollection<Order>();

        public void LoadMockupData()
        {
            
        }
    }
}
