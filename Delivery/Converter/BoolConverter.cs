﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Delivery.Converter
{
    class BoolConverter : IValueConverter
    {
        public string[] Variants { get; } = { "Yes", "No" };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((value as bool?) ?? false) ? "Yes" : "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.ToString() == "Yes";
        }
    }
}
