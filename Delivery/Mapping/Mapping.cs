﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Delivery.Model;
using Delivery.ViewModel;

namespace Delivery.Mapping
{
    public class Mapping
    {
        public static IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MainModel, MainViewModel>();
                cfg.CreateMap<MainViewModel, MainModel>();

                cfg.CreateMap<Courier, CourierViewModel>();
                cfg.CreateMap<CourierViewModel, Courier>();

                cfg.CreateMap<Customer, CustomerViewModel>();
                cfg.CreateMap<CustomerViewModel, Customer>();

                cfg.CreateMap<Order, OrderViewModel>();
                cfg.CreateMap<OrderViewModel, Order>();
            });
            return config.CreateMapper();
        }
    }
}
