﻿using System.Collections.ObjectModel;
using System.Media;
using System.Windows.Input;
using Delivery.Command;

namespace Delivery.ViewModel
{
    class MainViewModel : BaseViewModel
    {
        private static int _nextId;
        private ObservableCollection<CourierViewModel> _couriers;
        private ObservableCollection<CustomerViewModel> _customers;
        private ObservableCollection<OrderViewModel> _orders;
        private string _visibleControl = "Orders";

        public ObservableCollection<CourierViewModel> Couriers
        {
            get => _couriers;
            set
            {
                if (_couriers == value) return;
                _couriers = value;
                OnPropertyChanged(nameof(Couriers));
            }
        }

        public ObservableCollection<CustomerViewModel> Customers
        {
            get => _customers;
            set
            {
                if (_customers == value) return;
                _customers = value;
                OnPropertyChanged(nameof(Customers));
            }
        }

        public ObservableCollection<OrderViewModel> Orders
        {
            get => _orders;
            set
            {
                if (_orders == value) return;
                _orders = value;
                OnPropertyChanged(nameof(Orders));
            }
        }

        public string VisibleControl
        {
            get => _visibleControl;
            set
            {
                if (_visibleControl == value) return;
                _visibleControl = value;
                OnPropertyChanged(nameof(VisibleControl));
            }
        }

        public ICommand ChangeControlVisibility { get; set; }

        public MainViewModel()
        {
            Id = _nextId++;
            ChangeControlVisibility = new DelegateCommand<object>(ControlVisibility);
        }

        public void ControlVisibility(object control)
        {
            VisibleControl = control.ToString();
        }
    }
}
