﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ViewModel
{
    class CourierViewModel : PersonViewModel
    {
        private static int _nextId = 0;
        public CourierViewModel() { Id = _nextId++; }

        private string _carLicensePlate;
        
        public string CarLicensePlate
        {
            get => _carLicensePlate;
            set
            {
                if (_carLicensePlate == value) return;
                _carLicensePlate = value;
                OnPropertyChanged(nameof(CarLicensePlate));
            }
        }
    }
}
