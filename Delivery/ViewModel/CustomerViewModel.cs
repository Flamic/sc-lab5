﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ViewModel
{
    class CustomerViewModel : PersonViewModel
    {
        private static int _nextId = 0;
        public CustomerViewModel() { Id = _nextId++; }

        private bool _blacklisted;

        public bool Blacklisted
        {
            get => _blacklisted;
            set
            {
                if (_blacklisted == value) return;
                _blacklisted = value;
                OnPropertyChanged(nameof(Blacklisted));
            }
        }
    }
}
