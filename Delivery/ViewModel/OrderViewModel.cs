﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ViewModel
{
    class OrderViewModel : BaseViewModel
    {
        private static int _nextId = 0;
        public OrderViewModel() { Id = _nextId++; }

        private int _courierId;
        private int _customerId;
        private string _product;
        private string _address;
        private double _cost;
        private DateTime _receiveTime;

        public int CourierId
        {
            get => _courierId;
            set
            {
                if (_courierId == value) return;
                _courierId = value;
                OnPropertyChanged(nameof(CourierId));
            }
        }

        public int CustomerId
        {
            get => _customerId;
            set
            {
                if (_customerId == value) return;
                _customerId = value;
                OnPropertyChanged(nameof(CustomerId));
            }
        }

        public string Product
        {
            get => _product;
            set
            {
                if (_product == value) return;
                _product = value;
                OnPropertyChanged(nameof(Product));
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                if (_address == value) return;
                _address = value;
                OnPropertyChanged(nameof(Address));
            }
        }

        public double Cost
        {
            get => _cost;
            set
            {
                if (Math.Abs(_cost - value) < 0.01) return;
                _cost = value;
                OnPropertyChanged(nameof(Cost));
            }
        }

        public DateTime ReceiveTime
        {
            get => _receiveTime;
            set
            {
                if (_receiveTime == value) return;
                _receiveTime = value;
                OnPropertyChanged(nameof(ReceiveTime));
            }
        }
    }
}
